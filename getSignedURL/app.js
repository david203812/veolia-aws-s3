'use strict'

const AWS = require('aws-sdk')
AWS.config.update({ region: process.env.AWS_REGION })
const s3 = new AWS.S3()

// Change this value to adjust the signed URL's expiration.
const URL_EXPIRATION_SECONDS = 300

// Allowed file types.
const ALLOWED_MIME_TYPES = {
  'image/jpeg': 'jpg',
  'image/png': 'png',
  'audio/mpeg': 'mp3',
  'video/mp4': 'mp4'
}

/**
 * Get random string.
 */
function getRandomString() {
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
  let pwd = ''
  for (let i = 0; i < 8; ++i) {
    pwd += chars.charAt(Math.floor(Math.random() * 52))
  }
  return pwd
}

async function getUploadURL(event) {
  const randomID = getRandomString()
  const mimeType = event.queryStringParameters.mime
  const suffix = ALLOWED_MIME_TYPES[mimeType]

  if (!suffix) throw new Error('Content type is not allowed!')

  const key = `${Date.now()}${randomID}.${suffix}`

  // Get signed URL from S3
  const s3Params = {
    Bucket: process.env.UploadBucket,
    Key: key,
    Expires: URL_EXPIRATION_SECONDS,
    ContentType: mimeType,

    // This ACL makes the uploaded object publicly readable. You must also uncomment
    // the extra permission for the Lambda function in the SAM template.
    ACL: 'public-read'
  }

  const uploadURL = await s3.getSignedUrlPromise('putObject', s3Params)

  const bucketURL = `https://${process.env.UploadBucket}.s3.${process.env.AWS_REGION}.amazonaws.com`

  return JSON.stringify({ uploadURL, key, bucketURL })
}

// Main Lambda entry point
exports.handler = async (event) => {
  return await getUploadURL(event)
}
