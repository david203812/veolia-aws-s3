# veolia-aws-s3

## Docs

- https://aws.amazon.com/cn/blogs/compute/uploading-to-amazon-s3-directly-from-a-web-or-mobile-application

## From

[amazon-s3-presigned-urls-aws-sam](https://github.com/aws-samples/amazon-s3-presigned-urls-aws-sam)

## Requirements

* AWS CLI already configured with Administrator permission.
* [AWS SAM CLI installed](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html) - minimum version 0.48.

## Steps

1. Configure Amazon Web Services credential.
```shell
docker run --rm -it -v ~/.aws:/root/.aws amazon/aws-cli configure
```

2. Deploy app.
```shell
sam deploy --guided
```

## Use docker
```shell
docker pull python:3.8

docker run --name=aws-s3 -dit --volume=/root/active/veolia/deploy/aws-s3:/usr/src/aws-s3 --volume=/root/.aws:/root/.aws python:3.8

docker cp /root/active/aws-sam-cli-linux-x86_64.zip aws-s3:/usr/src/aws-sam-cli.zip

docker exec -it aws-s3 bash

cd /usr/src

unzip aws-sam-cli.zip 

cd aws-sam-cli-src

pip install -i https://pypi.douban.com/simple -r requirements/base.txt

python setup.py install

cd ../aws-s3

sam deploy --guided
```
